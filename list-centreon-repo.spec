###############################################################################
# Spec file for list-centreon-repo
################################################################################
# Configured to be built by other non-root user
################################################################################
#

Summary: script to list centreon github repositories
Name: list-centreon-repo
Version: 1.0.0
Release: 1
License: GPL
Group: System
Packager: Alexis Marchand
Requires: bash
Requires: epel-release
Requires: jq
BuildRoot: ~/rpmbuild/

%description
A script to list centreon github repositories.

%prep
################################################################################
# Create the build tree and copy the files from the development directories    #
# into the build tree.                                                         #
################################################################################
echo "BUILDROOT = $RPM_BUILD_ROOT"
mkdir -p $RPM_BUILD_ROOT/usr/local/bin/

cp /home/builder/list-centreon-repo.sh $RPM_BUILD_ROOT/usr/local/bin

exit

%files
%attr(0755, root, root) /usr/local/bin/list-centreon-repo.sh

%pre

%post

%postun

%clean
rm -rf $RPM_BUILD_ROOT/usr/local/bin


%changelog
* Sat Sep 25 2021 Alexis Marchand
  - The original package includes list-centreon-repo script
