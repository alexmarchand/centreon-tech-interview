#!/bin/bash
# a simple bash script to list centreon repositories on github

curl -H "Accept: application/vnd.github.v3+json" https://api.github.com/orgs/centreon/repos \
    | jq .[].name
