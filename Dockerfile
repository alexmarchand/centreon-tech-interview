FROM centos:7

RUN yum install rpm-build -y

RUN useradd builder -u 1000 -m -G users,wheel

USER builder
WORKDIR /home/builder/
ENTRYPOINT ["rpmbuild", "--target", "noarch", "--bb", "list-centreon-repo.spec"]